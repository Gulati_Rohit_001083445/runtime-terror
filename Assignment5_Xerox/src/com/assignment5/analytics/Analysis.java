/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import com.assignment5.xerox.UpdateProduct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.text.DecimalFormat;

/**
 *
 * @author mayur
 */
public class Analysis {
    
    public void getTopThreeBestNegotiatedProducts(){
        Map<Integer, Order> order = DataStore.getInstance().getOrders();
        Map<Integer, Product> product = DataStore.getInstance().getProducts();
        Map<Integer, Integer> productQuantity = new HashMap<>();
        int totalQuantity = 0;
        
        for(Product p:product.values()){       
            for (Order o: order.values()){
                if(o.getItem().getProductId() == p.getProductId()){
                    if(o.getItem().getSalesPrice() > p.getTargetPrice()){
                        totalQuantity += o.getItem().getQuantity();
                    }
                }
            }
            productQuantity.put(p.getProductId(), totalQuantity);
            totalQuantity = 0;
        }
        
        List<Map.Entry<Integer, Integer> > list = new LinkedList<Map.Entry<Integer, Integer> >(productQuantity.entrySet()); 
        
        Collections.sort(list,new Comparator<Map.Entry<Integer, Integer> >() {
            @Override
            public int compare(Map.Entry<Integer, Integer> v1, Map.Entry<Integer, Integer> v2) {
                return Double.compare(v2.getValue(),v1.getValue()); 
            }
        });
        
        HashMap<Integer, Integer> sortedList = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer>  v: list) { 
            sortedList.put(v.getKey(), v.getValue()); 
        } 
        
        int count = 0;
        for(Map.Entry<Integer, Integer> en : sortedList.entrySet()){
               System.out.println("ProductId = " + en.getKey() +  
                          ", Quantity = " + en.getValue()); 
               if(count == 2){
                   break;
               }
               count++;
            }
 
        
       } 
    
 
    public void getTopThreeBestCustomers(){
        Map<Integer,Customer> customers = DataStore.getInstance().getCustomers();
        ArrayList<Customer> customerList = new ArrayList<>();
        double absoluteSum=0;
        for(Customer c: customers.values()){
            for(Order o: c.getOrderList()){
                Item i = o.getItem();
                Product p = o.getProduct();
                
                absoluteSum += Math.abs(i.getSalesPrice() - p.getTargetPrice());
                c.setAbsoluteSum(absoluteSum);
            }
            customerList.add(c);
            absoluteSum =0;
        }
        
        Collections.sort(customerList,new Comparator<Customer>() {
            @Override
            public int compare(Customer c1, Customer c2) {
                return Double.compare(c1.getAbsoluteSum(), c2.getAbsoluteSum());
                //return Double.compare(c2.getAbsoluteSum(), c1.getAbsoluteSum());
            }
        });
        
       
        for(int i=0;i<customerList.size() && i<3;i++){
             System.out.println(customerList.get(i));
        }
    }
    
    public void threeSalesPeopleWithMostProfit(){
        
        Map<Integer, SalesPerson> salesPerson = DataStore.getInstance().getSalesPersons();
        ArrayList<SalesPerson> person = new ArrayList<SalesPerson>();
        
        double totalProfit = 0;
        
        for(SalesPerson s: salesPerson.values())
        {
            for(Order o: s.getOrderList()){
                Item i = o.getItem();
                Product p = o.getProduct();
                
                if(i.getSalesPrice() > p.getTargetPrice()){
                    totalProfit += ((i.getSalesPrice() - p.getTargetPrice())* i.getQuantity());
                    s.setTotalProfit(totalProfit);
                }

            }
            person.add(s);
            totalProfit=0;
        }
        
        
        Collections.sort(person, new Comparator<SalesPerson>() {
            @Override
            public int compare(SalesPerson s1, SalesPerson s2) {
                return Double.compare(s2.getTotalProfit(), s1.getTotalProfit());
            }
        });
         for(int i=0;i<person.size() && i<3;i++){
             System.out.println(person.get(i));
        }
    }
    
    public void totalProfitByEverySalesPerson(){
        Map<Integer, SalesPerson> salesPerson = DataStore.getInstance().getSalesPersons();
        double OverallProfit = 0;
        for (SalesPerson s : salesPerson.values()) {
            OverallProfit += s.getTotalProfit();
        }
        
        System.out.println("Total Profit  " + OverallProfit);
    }
    
    public void performanceManagement(){
        Map<Integer, Order> order = DataStore.getInstance().getOrders();
        Map<Integer, Product> product = DataStore.getInstance().getProducts();
        ArrayList<Product> originalProductList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("0.00");
        float totalSalesPrice = 0.0F;
        float avgSalesPrice = 0.0F;
        float difference =0.0F;
        float error = 0.0f;
        int count = 0;
        for(Product p: product.values()){
            for(Order o:order.values()){
                if(o.getItem().getProductId() == p.getProductId()){
                    totalSalesPrice += o.getItem().getSalesPrice();
                    count++;
                }
            }
            avgSalesPrice = totalSalesPrice/count;
            count =0;
            totalSalesPrice = 0;
            p.setAvgSalesPrice(avgSalesPrice);
            difference = (float)(p.getAvgSalesPrice() - p.getTargetPrice());
            p.setDifference(difference);
            error = (float)((p.getTargetPrice() - p.getAvgSalesPrice())/p.getAvgSalesPrice())*100;
            p.setError(error);
//            if(error < -1){
//                p.setModifiedTargetPrice(avgSalesPrice-(0.01*avgSalesPrice));
//            }
//            else if(error > 1){
//                p.setModifiedTargetPrice(avgSalesPrice+(0.01*avgSalesPrice));
//            }
            if(error < -5){
                p.setModifiedTargetPrice(avgSalesPrice-(0.05*avgSalesPrice));
            }
            else if(error > 5){
                p.setModifiedTargetPrice(avgSalesPrice+(0.05*avgSalesPrice));
            }
            else{
                p.setModifiedTargetPrice(p.getTargetPrice());
            }
            originalProductList.add(p);
        }
        
        Collections.sort(originalProductList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return Double.compare(p2.getDifference(),p1.getDifference());
            }
        });
        
        
       
        System.out.println("\n--------------Original Data--------------\n");
        System.out.println("--------------Section1: Average sales price greater than target price--------------");
        System.out.println("ProductId " + " AverageSalesPrice "+ " TargetPrice " + "   Difference" + "  \t Error\n");
        for(Product prod:originalProductList){
           
            if(prod.getDifference() > 0){
                System.out.println(prod.getProductId() + "\t   " + df.format(prod.getAvgSalesPrice())+ "\t \t" + df.format(prod.getTargetPrice()) + "\t \t" + df.format(prod.getDifference()) + "\t \t"+ df.format(prod.getError())+"%");
            }             
        }
        System.out.println("--------------Section2: Average sales price lower than or equal to target price--------------");
        System.out.println("ProductId " + " AverageSalesPrice "+ " TargetPrice " + "   Difference"+ " \t  Error\n");
        for(Product prod:originalProductList){
           
            if(prod.getDifference() <= 0){
                System.out.println(prod.getProductId() + "\t   " + df.format(prod.getAvgSalesPrice())+ "\t \t" + df.format(prod.getTargetPrice()) + "\t \t" + df.format(prod.getDifference()) + "\t \t"+ df.format(prod.getError())+"%");
            }             
        }
        
        
        for(Product modProd:originalProductList){
            difference = (float)(modProd.getAvgSalesPrice() - modProd.getModifiedTargetPrice());
            modProd.setDifference(difference);
            error = (float)((modProd.getModifiedTargetPrice() - modProd.getAvgSalesPrice())/modProd.getAvgSalesPrice())*100;
            modProd.setError(error);
        }
        
         Collections.sort(originalProductList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return Double.compare(p2.getDifference(),p1.getDifference());
            }
        });
         
        System.out.println("\n--------------Modified Data--------------\n");
        System.out.println("--------------Section1: Average sale price greater than modified target price--------------");
        System.out.println("ProductId " + " AverageSalesPrice "+ " ModifiedTargetPrice " +" Difference"+ "   Error\n");
        for(Product modProd:originalProductList){
            if(modProd.getDifference() > 0){
                System.out.println(modProd.getProductId() + "\t   " + df.format(modProd.getAvgSalesPrice())+ "\t \t" + df.format(modProd.getModifiedTargetPrice())+ " \t \t    " + df.format(modProd.getDifference()) +"\t"+ df.format(modProd.getError())+"%");
            }
            
        }
        
        System.out.println("--------------Section2: Average sales price lower than or equal to modified target price--------------");
        System.out.println("ProductId " + " AverageSalesPrice "+ " ModifiedTargetPrice " +" Difference"+ "   Error\n");
        for(Product modProd:originalProductList){
            if(modProd.getDifference() <= 0){
                System.out.println(modProd.getProductId() + "\t   " + df.format(modProd.getAvgSalesPrice())+ "\t \t" + df.format(modProd.getModifiedTargetPrice())+ " \t \t    " + df.format(modProd.getDifference()) +"\t"+ df.format(modProd.getError())+"%");
            }
            
        }
        try{
            UpdateProduct prod = new UpdateProduct(originalProductList);
        }
        catch(IOException e){
             System.out.println("Error while flushing/closing fileWriter !!!");
             e.printStackTrace();
        }
        
    }
 }
