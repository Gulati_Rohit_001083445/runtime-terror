/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Customer {
    
    private int customerId;
    private List<Order> orderList;
    private double absoluteSum;
    
    public Customer(int customerId){
        this.customerId = customerId;
        this.orderList = new ArrayList<>();
        
        
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public double getAbsoluteSum() {
        return absoluteSum;
    }

    public void setAbsoluteSum(double absoluteSum) {
        this.absoluteSum = absoluteSum;
    }
    
    @Override
    public String toString(){
        return "Customer data{ customerId: "+this.customerId+", absoluteSum: "+this.absoluteSum+"}";
    }
}
