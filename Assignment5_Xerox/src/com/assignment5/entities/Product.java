/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Product {
    private int productId;
    private double minPrice;
    private double maxPrice;
    private double targetPrice;
    private int quantitySold;
    private float avgSalesPrice;
    private float difference;
    private float error;
    private double modifiedTargetPrice;
    
    public Product(int productId, double minPrice, double maxPrice, double targetPrice) {
        this.productId = productId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.targetPrice = targetPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }

    public float getAvgSalesPrice() {
        return avgSalesPrice;
    }

    public void setAvgSalesPrice(float avgSalesPrice) {
        this.avgSalesPrice = avgSalesPrice;
    }

    public float getDifference() {
        return difference;
    }

    public void setDifference(float difference) {
        this.difference = difference;
    }

    public float getError() {
        return error;
    }

    public void setError(float error) {
        this.error = error;
    }

    public double getModifiedTargetPrice() {
        return modifiedTargetPrice;
    }

    public void setModifiedTargetPrice(double modifiedTargetPrice) {
        this.modifiedTargetPrice = modifiedTargetPrice;
    }
    
    
    
    
    @Override
    public String toString() {
        
        //double order_total = item.quantity * item.salesPrice;
        return "Product{" + "id = " + productId + ", min price = " + minPrice + ", max price = " + maxPrice + " target price ="+targetPrice+" quantity sold = "+quantitySold+" Average Sales Price = "+avgSalesPrice+" Difference"+difference+" Error = "+error+" modifiedTargetPrice = "+modifiedTargetPrice+'}';
    }
    
}
