/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author kasai
 */
public class Order {
    
    int orderId;
    int salesPersonId;
    int customerId;
    Item item;
    private String marketSegment;
    private Product product;

    public Order(int orderId, int salesPersonId, int customerId, Item item, String marketSegment,Product product) {
        this.orderId = orderId;
        this.salesPersonId = salesPersonId;
        this.customerId = customerId;
        this.item = item;
        this.marketSegment = marketSegment;
        this.product = product;
    }

    public String getMarketSegment() {
        return marketSegment;
    }

    public void setMarketSegment(String marketSegment) {
        this.marketSegment = marketSegment;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
    
     @Override
    public String toString(){
        return "Order data{ orderId: "+this.orderId+", salesPersonId: "+this.salesPersonId+", customerId: "+this.customerId +", marketSegment: "+this.marketSegment+", item:{"+this.item+"}, product:{"+this.product+"}}";
    }
}
