/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class SalesPerson {
    private int salesPersonId;
    private List<Order> orderList;
    private double totalProfit;
    
    public SalesPerson(int salesPersonId){
        this.salesPersonId = salesPersonId;
        this.orderList = new ArrayList<>();
    }

    public int getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(double totalProfit) {
        this.totalProfit = totalProfit;
    }
    
    @Override
    public String toString(){
        return "Sales Person data{ salesPersonId: "+this.salesPersonId+", totalProfit: "+this.totalProfit+"}";
    }
}
