/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Product;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author mayur
 */
public class UpdateProduct {
    private FileWriter writer;
    private File file;
    private ArrayList<Product> prod;
    private final String PRODUCT_HEADER = "Product-Id,Min-Price,Max-Price,Target-Price,Modified-Target-Price";
    DataReader orderReader;
    DataReader productReader;
    private final String PROD_CAT_PATH = "./ProductCatalogueModified.csv";
    private final String LINE_BREAK = "\n";
    public UpdateProduct(ArrayList<Product> prod) throws IOException{
        this.prod = prod;
        productReader = new DataReader(PROD_CAT_PATH);
        updateTargetPrice(prod);
    }
    
    private void updateTargetPrice(ArrayList<Product> prod) throws IOException{
        try {
            double minPrice = 0;
            double maxPrice = 0;
            double targetPrice = 0;
            double modifiedTargetPrice = 0;
            file = new File(PROD_CAT_PATH);
            if(file.exists()){
                file.delete();
            }
            file.createNewFile();
            writer = new FileWriter(file);
            
            writer.append(PRODUCT_HEADER);
            writer.append(LINE_BREAK);
            
            //items for a order.
            for(Product p:prod){
            minPrice = p.getMinPrice();
            maxPrice = p.getMaxPrice();
            targetPrice = p.getTargetPrice();
            modifiedTargetPrice = p.getModifiedTargetPrice();
            String column = p.getProductId()+","+minPrice+","+maxPrice+","+targetPrice+","+modifiedTargetPrice;
            
            writer.append(column);
            writer.append(LINE_BREAK);
            }
            
            
        }finally{
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }
    
}
