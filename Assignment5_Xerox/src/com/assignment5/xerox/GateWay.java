/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.analytics.Analysis;
import com.assignment5.analytics.DataStore;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author kasai
 */
public class GateWay {
    
    DataReader orderReader;
    DataReader productReader;
    Analysis helper;

    public GateWay() throws IOException  {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new Analysis();
    }
    
    
/*    public static void main(String args[]) throws IOException{
        
        DataGenerator generator = DataGenerator.getInstance();
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            printRow(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }*/
    
    public static void main(String[] args) throws IOException {     
        GateWay inst = new GateWay();
        inst.readData();
    }
    
    private void readData() throws IOException{
        String[] row;

        while((row = productReader.getNextRow()) != null ){
            
            generateProduct(row);
        }
        
        while ((row = orderReader.getNextRow()) != null) {
            Order o = generateOrders(row);
            generateCustomers(row, o);
            generateSalesPersons(row, o);       
        }
        
        runAnalysis();
    }
    
    private void generateProduct(String[] row){
        int productID = Integer.parseInt(row[0]);
        double minPrice = Double.parseDouble(row[1]);
        double maxPrice = Double.parseDouble(row[2]);
        double targetPrice = Double.parseDouble(row[3]);
        
        Product prod = new Product(productID, minPrice,maxPrice,targetPrice);
        DataStore.getInstance().getProducts().put(productID, prod);
        
    }
    
    private Order generateOrders(String[] row){
        int orderId = Integer.parseInt(row[0]);
        int salesPersonId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        int productId = Integer.parseInt(row[2]);
        double salesPrice = Double.parseDouble(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item item = new Item(productId, salesPrice, quantity);
        String marketSegment = row[7];
                
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        
        Order order = new Order(orderId, salesPersonId, customerId, item, marketSegment, products.get(productId));
        DataStore.getInstance().getOrders().put(orderId, order);
        return order;
        
    }
    
    private static void generateCustomers(String[] row, Order o){
        int customerId = Integer.parseInt(row[5]);
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        if(!customers.containsKey(customerId)){
            Customer c = new Customer(customerId);
            customers.put(customerId, c);
        }
        if(customers.containsKey(customerId)){
            customers.get(customerId).getOrderList().add(o);
        }
    }
    
    private static void generateSalesPersons(String[] row, Order o){
        int salesPersonId = Integer.parseInt(row[4]);
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPersons();
        if(!salesPersons.containsKey(salesPersonId)){
            SalesPerson s = new SalesPerson(salesPersonId);
            salesPersons.put(salesPersonId, s);
        }
        if(salesPersons.containsKey(salesPersonId)){
            salesPersons.get(salesPersonId).getOrderList().add(o);
        }
    }
    
    
    private void runAnalysis(){
        System.out.println("\n------ 1. Our top 3 best negotiated products------\n");
        helper.getTopThreeBestNegotiatedProducts();
        System.out.println("\n------ 2. Our top 3 best Customers------\n");
        helper.getTopThreeBestCustomers();
        System.out.println("\n------ 3. Our top 3 best SalesPerson------\n");
        helper.threeSalesPeopleWithMostProfit();
        System.out.println("\n------ 4. Overall profit made by every Sales Person ------\n");
        helper.totalProfitByEverySalesPerson();
        System.out.println("\n------ 5. Performace analysis of each product ------\n");
        helper.performanceManagement();
        
    }
    
}
