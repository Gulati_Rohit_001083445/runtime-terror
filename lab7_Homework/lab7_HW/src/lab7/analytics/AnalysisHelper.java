/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO

    // find 5 comments which have the most likes
    // TODO
    public void getFiveLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>() {

            public int compare(Comment c1, Comment c2) {
                return Double.compare(c2.getLikes(), c1.getLikes());

            }
        });

        for (int i = 0; i < comments.size() && i < 5; i++) {
            System.out.println(commentList.get(i));
        }

    }

    public void getAvgLikesPerComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<Comment> commentList = new ArrayList<>(comments.values());
        int numOfComments = commentList.size();
        float avgLikes = 0.0F;
        int sumOfLikes = 0;
        for (Comment c : commentList) {
            sumOfLikes += c.getLikes();
        }

        avgLikes = (float) sumOfLikes / numOfComments;
        System.out.println("1. Average Likes per comment:" + avgLikes);
    }

    public void postWithMostLikedComments() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<Comment> commentList = new ArrayList<>(comments.values());
        Collections.sort(commentList, new Comparator<Comment>() {

            public int compare(Comment c1, Comment c2) {
                return Double.compare(c2.getLikes(), c1.getLikes());

            }
        });
        int highestLike = commentList.get(0).getLikes();
        HashSet<Comment> mostLikedPosts = new HashSet<Comment>();
        for (int i = 0; i < comments.size(); i++) {
            if (commentList.get(i).getLikes() == highestLike) {
                mostLikedPosts.add(commentList.get(i));
            }
        }
        System.out.println("2. Post with most liked comments");
        for (Comment c : mostLikedPosts) {
            System.out.println(c);
        }

    }

    public void postWithMostComments() {
        Map<Integer, Integer> commentsCount = new HashMap<>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();

        for (Post post : posts.values()) {
            commentsCount.put(post.getPostId(), post.getComments().size());
        }
        int max = 0;
        int maxId = 0;

        for (int id : commentsCount.keySet()) {

            if (commentsCount.get(id) > max) {
                max = commentsCount.get(id);
                maxId = id;
            }
        }

        System.out.println("3. Post with Most Comments : " + max + "\n" + posts.get(maxId));
    }
    
    public void topFiveInactiveUserbyTotalPost(){
        
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        ArrayList<User> user = new ArrayList<User>();

        for (User u : users.values()) {
            int count = 0;
            for (Post post : posts.values()) {
                if (u.getId() == post.getUserId()) {
                    count++;
                }
            }
            u.setTotalPosts(count);
            user.add(u);
        }

        Collections.sort(user, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getTotalPosts() - o2.getTotalPosts();
            }
        });

        System.out.println("4. Top Five Inactive User By Total Posts : ");
        for (int i = 0; i < user.size() && i<5; i++) {
            System.out.println(user.get(i));
        }
        
    }
    
    public void topFiveInactiveUserbyTotalComments(){
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        List<User> commentList = new ArrayList<>(users.values());
        
         
         Collections.sort(commentList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return  o1.getComments().size() - o2.getComments().size();
            } 
        });  
         
         System.out.println("5. Top 5 inactive users based on comments:");
        for(int i = 0; i<commentList.size() && i <5; i++){
       System.out.println(commentList.get(i));
   }
    }
    
    public void topFiveInactiveUsers(){
         Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        ArrayList<User> newUserList = new ArrayList<>();
        for(User u: users.values()){
            int commentSize = u.getComments().size();
            int postUserSize = 0;
            for(Post p: posts.values()){
                if(p.getUserId() == u.getId()){
                    postUserSize++;
                }
            }
            u.setTotalCount((commentSize + postUserSize));
            newUserList.add(u);
        }
        
        Collections.sort(newUserList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2){
                return o1.getTotalCount()- o2.getTotalCount();
            }
        });
        System.out.println("6. Top 5 inactive users based on sum of comments, Posts and Likes:");
        for(int i=0; i < newUserList.size() && i < 5; i++){
            System.out.println("Top "+(i+1)+" inactive user: " + newUserList.get(i));
        }
        
    }
    
    public void topFiveProactiveOverall(){
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        ArrayList<User> newUserList = new ArrayList<>();
        for(User u: users.values()){
            int commentSize = u.getComments().size();
            int postUserSize = 0;
            for(Post p: posts.values()){
                if(p.getUserId() == u.getId()){
                    postUserSize++;
                }
            }
            u.setTotalCount((commentSize + postUserSize));
            newUserList.add(u);
        }
        
        Collections.sort(newUserList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2){
                return o2.getTotalCount() - o1.getTotalCount();
            }
        });
        System.out.println("7. Top 5 proactive users based on sum of comments, Posts and Likes:");
        for(int i=0; i < newUserList.size() && i < 5; i++){
            System.out.println("Top "+(i+1)+" proactive user: " + newUserList.get(i));
        }
    }
}